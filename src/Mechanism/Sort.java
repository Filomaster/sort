package Mechanism;

public class Sort implements Data{

    private boolean isInOrder = true;
    private boolean temp = false;

    public void sort() {
        int value1;
        int value2;

        int licznik = 0;
        System.out.println("############### SORTOWANIE ################");
        System.out.println("");
        //check
        do {
            System.out.println(" ------ Przebieg "+licznik+" ------");
            int counter = 0;
            do {
                isInOrder = true;
                if (data[counter] > data[counter + 1]) {
                    isInOrder = false;
                    temp = true;
                    System.out.println("Liczby nie są ułożone kolejno");

                } else {
                    counter = counter + 2;
                }

            } while (!isInOrder || counter < count-2 );
            //sort
            if(temp) {
                for (int i = 0; i < count / 2; i = i + 2) {
                    if (data[i] > data[i + 1]) {
                        value1 = data[i];
                        value2 = data[i + 1];

                        data[i] = value2;
                        data[i + 1] = value1;
                        System.out.println(" Zamieniono pozycje "+i+" i "+(i+1)+" Aktualne wartości:");
                        System.out.println("Pozycja "+i+": "+data[i]);
                        System.out.println("Pozycja "+(i+1)+": "+data[i+1]);
                        System.out.println("");
                    }
                }
            }
            licznik++;
            System.out.println("");
        }while(!isInOrder);
    }
}
