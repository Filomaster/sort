package Mechanism;

public class Shuffle implements Data{

    public static void shuffle (){

        int value1;
        int value2;

        int position1;
        int position2;

        //reset();
        Data_manager.setup();
        System.out.println("############ SHUFFLE ##################");
        System.out.println("");
        for(int i = 0; i < count/2; i ++){
            position1 = Data_manager.getRandom(i);
            position2 = Data_manager.getRandom((count-1)-i);

            //System.out.println("------- RUN "+i+" -------");
            //System.out.println("Position: "+position1+" Initial Value: "+data[position1]);
            //System.out.println("Position: "+position2+" Initial Value: "+data[position2]);

            value1 = data[position1];
            value2 = data[position2];

            data[position1] = value2;
            data[position2] = value1;

            //System.out.println("--------");
            //System.out.println("Position: "+position1+" Shuffled Value: "+data[position1]);
           // System.out.println("Position: "+position2+" Shuffled Value: "+data[position2]);
           // System.out.println("");

        }
        System.out.println("###########################################");
        System.out.println("");
    }

    public static void reset() {
        System.out.println("############ INITIALIZATION ###################");
        for (int i = 0; i < count; i++) {
            data[i] = (i * 5);
            System.out.println("Position: " + i + " Value: " + data[i]);
        }
        System.out.println("###########################################");
        System.out.println("");
    }

}
