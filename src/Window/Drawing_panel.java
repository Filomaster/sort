package Window;

import Mechanism.Data;
import Mechanism.Shuffle;
import Mechanism.Sort;

import javax.swing.*;
import java.awt.*;

public class Drawing_panel extends JPanel implements Data {

    public Drawing_panel(){
        setPreferredSize(new Dimension(1000, 500));
        //setBackground(Color.getHSBColor(2,3,4));
        Shuffle.reset();
        Shuffle.shuffle();
        Sort sort = new Sort();
        sort.sort();

    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D screen = (Graphics2D)g;

        for(int i = 0; i < count; i++) {
            screen.fillRect(i*10, 500-data[i], 10,data[i]);
            //System.out.println("Narysowano: "+i+" o długości: "+data[i]);
        }
    }

}
