package Window;

import Mechanism.Shuffle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button_panel extends JPanel implements ActionListener {
    private JButton shuffleButton;
    private JButton sortButton;
    public boolean shuffle= false;

    public Button_panel(){
        setPreferredSize(new Dimension(1000, 50));

        shuffleButton = new JButton("Shuffle");
        sortButton = new JButton("Sort");

        shuffleButton.addActionListener(this);
        sortButton.addActionListener(this);

        setLayout(new GridLayout(1,2));

        add(shuffleButton);
        add(sortButton);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object source = e.getSource();

        if(source == shuffleButton){
            System.out.println("shuffle");
            Shuffle.shuffle();
            shuffle = true;
        }

    }
}
