package Window;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public Frame(){
        super("Sorter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(1000,600);
        setResizable(false);
        setLayout(new BorderLayout());

        JPanel visualization = new Drawing_panel();
        JPanel controls = new Button_panel();


        /*JMenuBar menu_b = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        menu.add(new JMenuItem("Item 1"), new JMenuItem("Item 2"));
        menu_b.add(menu);

        add(BorderLayout.PAGE_START,menu);*/


        add(BorderLayout.PAGE_START, visualization);
        add(BorderLayout.PAGE_END, controls);

        pack();



        //TO DO:
        //Add layout manager

    }
}
